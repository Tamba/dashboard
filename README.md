##Dashboard

This project is a dummy-version of the "dashboard" my coworkers and I build for viewing data and user statistics of our web platforms - CBioPortal and Matchminer - as well as the Profile Sequencing Initiative.  We, KSG at Dana Farber, have the dashboard up 24/7 in one of our meeting rooms.

I designed the interface and wrote the front-end code for the dashboard.

For the "real" dashboard, the one in-house, the spline graphs, line graphs, stack graphs, and tables show time-dependent data, such as gene search history and user growth, while bar graph and other graphics displayed current aggregation statistics, such as the number of curated present-day trial matches and most frequently sequenced cancer types.  The "real" dashboard is also hooked up to an API and makes `GET` requests every few hours.
 
For the purposes of this dummy display, all of the real query information has been removed and replaced with dummy data and titles and no API calls are being made.

![Dashboard 1](./public/dash1.png)

![Dashboard 2](./public/dash2.png)